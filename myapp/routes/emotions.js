var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render("emotions/index");
});

router.get('/dashboard', function(req, res, next) {
  res.render("emotions/dashboard");
});

module.exports = router;
