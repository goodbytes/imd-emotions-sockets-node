primus = Primus.connect("", {
  reconnect: {
      max: Infinity // Number: The max delay before we try to reconnect.
    , min: 500 // Number: The minimum delay before we try reconnect.
    , retries: 10 // Number: How many times we should try to reconnect.
  }
});

primus.on("data", function(data) {
    console.log(data);
    
    if(data.emotion != undefined){
        var emotion = data.emotion;
        console.log("we received the following emotion: " + emotion);
        // let's increase this emotion 
        var width = document.querySelector("li."+emotion+" img").style.width;
        var widthNumber = (width.substr(0, width.length-2))*1.1;

        document.querySelector("li."+emotion+" img").style.width = widthNumber+"px";
    }
    

})