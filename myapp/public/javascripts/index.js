primus = Primus.connect("", {
  reconnect: {
      max: Infinity // Number: The max delay before we try to reconnect.
    , min: 500 // Number: The minimum delay before we try reconnect.
    , retries: 10 // Number: How many times we should try to reconnect.
  }
});

primus.on("data", function(data) {
    console.log("PRIMUS CLIENT DATA RECEIVED!" + data);
})

document.querySelector(".emotions").addEventListener("click", function(e){
    target = e.target;
    var emo = target.parentNode.getAttribute("class");
    console.log(emo);
    primus.write({ emotion: emo }); // send our emotion to the server
    e.preventDefault();
});